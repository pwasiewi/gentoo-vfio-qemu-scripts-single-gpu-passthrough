#!/bin/bash

for a in $(find /sys/kernel/iommu_groups/ -type l | cut -d/ -f5,7 | sort -n); do echo IOMMU Group $(echo $a | cut -d/ -f1); lspci -nns $(echo $a | cut -d/ -f2); done
