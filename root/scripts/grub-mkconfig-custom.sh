#!/bin/bash

# update grub core and custom configuration files
oldversion=$(cat /etc/grub.d/40_custom | grep vmlinuz | cut -d '/' -f3 | cut -d ' ' -f1 | sed 's/vmlinuz-//' | sed 's/-x86_64//' | head --lines=1)
newversion=$(ls -ltra /usr/src/linux | cut -d '>' -f2 | sed 's/ linux-//g')

sed -i s/$oldversion/$newversion/g /etc/grub.d/40_custom

grub-mkconfig -o /boot/grub/grub.cfg
