#!/bin/bash

IFS=$'\n' array=($(ls --full-time --sort=time /boot/ | grep vmlinuz))

if [[ $1 == keep ]] || [[ $1 == remove ]]
then
	if [[ $1 == keep ]]
	then
		for (( i=0; i<=$(echo ${#array[@]})-1; i++ )); do echo [$i] ${array[$i]}; done
		read -p $'\nSelection: ' select
		if [[ $select =~ ^[0-9]+$ ]] && [ $select -ge 0 -a $select -lt ${#array[@]} ]
		then
			IFS=$'\n' arraydelete=($(for (( i=0; i<=$(echo ${#array[@]})-1; i++ )); do echo [$i] ${array[$i]}; done | grep --invert-match -- ${array[$select]} | sed 's/vmlinuz-//' | cut -d ' ' -f10))
			for (( i=0; i<=$(echo ${#arraydelete[@]})-1; i++ ))
			do
				if [[ ${arraydelete[$i]} == *".old"* ]]
				then
					rm --force --verbose /boot/initramfs-$(echo ${arraydelete[$i]} | sed 's/.old//').img.old
				fi
				rm --force --verbose /boot/initramfs-${arraydelete[$i]}.img
				rm --force --verbose /boot/System.map-${arraydelete[$i]}
				rm --force --verbose /boot/vmlinuz-${arraydelete[$i]}
			done
		else
			echo -e "\nInvalid selection.\n"
		fi
	fi
	if [[ $1 == remove ]]
	then
		for (( i=0; i<=$(echo ${#array[@]})-1; i++ )); do echo [$i] ${array[$i]}; done
		read -p $'\nSelection: ' select
		if [[ $select =~ ^[0-9]+$ ]] && [ $select -ge 0 -a $select -lt ${#array[@]} ]
		then
			arraydelete=$(echo ${array[$select]} | sed 's/vmlinuz-//' | cut -d ' ' -f10)
			if [[ $arraydelete == *".old"* ]]
			then
				rm --force --verbose /boot/initramfs-$(echo $arraydelete | sed 's/.old//').img.old
			fi
			rm --force --verbose /boot/initramfs-$arraydelete.img
			rm --force --verbose /boot/System.map-$arraydelete
			rm --force --verbose /boot/vmlinuz-$arraydelete
		else
			echo -e "\nInvalid selection.\n"
		fi
	fi
else
	echo -e "\nRemove specific kernel or keep selected kernel only.\n\n\tremovekernel.sh <argument>\n\n\tkeep\t\tKeep selected kernel only.\n\tremove\t\tRemove selected kernel.\n"
fi
