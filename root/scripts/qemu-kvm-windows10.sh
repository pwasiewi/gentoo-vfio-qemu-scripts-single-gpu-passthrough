#!/bin/bash

# disable efi framebuffer
if [[ -e /sys/bus/platform/drivers/efi-framebuffer/efi-framebuffer.0 ]]
then
	echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind
fi

# amd reset bug workaround
sleep 3
rtcwake --verbose --utc --mode mem --seconds 7
sleep 5

# path config
workdir=/root/scripts
vbios=$workdir/vega-vbios/vega64.rom
ovmfcode=$workdir/ovmf/OVMF_CODE.fd
mkdir --parents --verbose /mnt/hugetlbfs
hugetlbfs=/mnt/hugetlbfs
iso=/home/sdb2/installers/windows10-1607/windows10-1607.iso
virtio=/home/sdb2/installers/virtio-windows-drivers/virtio-win-0.1.171.iso
rootdisk=/home/nvme0n1p4/qemu-img/windows10-rootdisk.img
userdisk1=/home/md0/windows10-userdisk1.img
userdisk2=/home/sda2/windows10-userdisk2.img

# performance options
ulimit_original=$(ulimit -l)
ulimit_target=$((28*1024*1024))
ulimit -l $ulimit_target
mkdir --verbose --parents $hugetlbfs
mount --verbose --types hugetlbfs hugetlbfs $hugetlbfs
sysctl vm.nr_hugepages=1024

# start qemu kvm vm
qemu-system-x86_64 \
	-nodefaults -nographic -display none -vga none -monitor stdio -enable-kvm -accel kvm -name win10,debug-threads=on \
	-m 26G -mem-prealloc -object memory-backend-file,id=mem1,size=2G,align=2M,mem-path=$hugetlbfs,share=off,discard-data=on,merge=on,prealloc=on \
	-machine pc,accel=kvm,kernel_irqchip=on,vmport=off \
	-no-reboot -boot menu=on \
	-rtc clock=host,base=localtime \
	-cpu host,kvm=off,svm=off,topoext,hv_relaxed,hv_spinlocks=0x1fff,hv_time,hv_vapic,hv_vendor_id=0xDEADBEEFFF,hv_vpindex,hv_synic,hv_stimer,hv_frequencies \
	-smp 10,sockets=1,cores=5,threads=2 \
	-device vfio-pci,host=28:00.0,multifunction=on,x-vga=on,romfile=$vbios \
	-device vfio-pci,host=28:00.1 \
	-device vfio-pci,host=2a:00.3 \
	-device qemu-xhci,id=xhci0 -device usb-host,bus=xhci0.0,hostbus=1,hostport=8 -device usb-host,bus=xhci0.0,hostbus=1,hostport=9 \
	-device qemu-xhci,id=xhci1 -device usb-host,bus=xhci1.0,hostbus=4,hostport=1 -device usb-host,bus=xhci1.0,hostbus=4,hostport=2 \
	-device qemu-xhci,id=xhci2 -device usb-host,bus=xhci2.0,hostbus=1,hostport=7 -device usb-host,bus=xhci2.0,hostbus=1,hostport=10 \
	-device qemu-xhci,id=xhci3 -device usb-host,bus=xhci3.0,hostbus=1,hostport=3 -device usb-host,bus=xhci3.0,hostbus=4,hostport=4 \
	-drive if=pflash,format=raw,readonly,file=$ovmfcode \
	-device virtio-scsi,id=scsi0 -device scsi-hd,bus=scsi0.0,drive=root -drive file=$rootdisk,id=root,format=raw,cache=none,aio=native,if=none \
	-device virtio-scsi,id=scsi1 -device scsi-hd,bus=scsi1.0,drive=user1 -drive file=$userdisk1,id=user1,format=raw,cache=none,aio=native,if=none \
	-device virtio-scsi,id=scsi2 -device scsi-hd,bus=scsi2.0,drive=user2 -drive file=$userdisk2,id=user2,format=raw,cache=none,aio=native,if=none \
	-device virtio-net,netdev=net0 -netdev tap,id=net0,ifname=tap0,script=no,downscript=no,vhost=on

# revert performance options
ulimit -l $ulimit_original
sysctl vm.nr_hugepages=0
umount --verbose $hugetlbfs
/bin/bash /root/scripts/qemu-revert-cpus.sh

# reboot host
if [[ -z $(who | grep -i -E 'ttyS|ttyUSB|pts') ]]; then reboot; else (echo -e "\nActive SSH Sessions:" && who); fi
