#1/bin/bash

if [[ $1 == fresh ]] || [[ $1 == existing ]] || [[ $1 == select ]]
then
	if [[ $1 == fresh ]]
	then
		mkdir --verbose --parents /etc/kernels
		curl --retry 3 --retry-delay 5 https://git.archlinux.org/svntogit/packages.git/plain/linux-lts/repos/core-x86_64/config --output /etc/kernels/kernel-config-archlinux
		if [[ -e /usr/src/linux/.config  ]]; then mv --verbose --force /usr/src/linux/.config /usr/src/linux/.config--$(date +%Y-%m-%d--%H-%M-%S).bak; fi
		genkernel --color --menuconfig --save-config --bcache --no-hyperv --microcode=amd --no-virtio --mrproper --no-splash --no-ramdisk-modules --no-nice --no-mdadm --no-lvm --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --disklabel --no-luks --busybox --no-unionfs --no-netboot --compress-initramfs --compress-initramfs-type=best --kernel-config=/etc/kernels/kernel-config-archlinux all
	fi
	if [[ $1 == existing ]]
	then
		genkernel --color --menuconfig --save-config --bcache --no-hyperv --microcode=amd --no-virtio --no-mrproper --no-clean --no-splash --no-ramdisk-modules --no-nice --no-mdadm --no-lvm --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --disklabel --no-luks --busybox --no-unionfs --no-netboot --compress-initramfs --compress-initramfs-type=best all
	fi
	if [[ $1 == select ]]
	then
		IFS=$'\n' array=($(ls --full-time --sort=time /etc/kernels/*))
		for (( i=0; i<=$(echo ${#array[@]})-1; i++ )); do echo [$i] ${array[$i]}; done
		read -p $'\nSelection: ' select
		if [[ $select =~ ^[0-9]+$ ]] && [ $select -ge 0 -a $select -lt ${#array[@]} ]
		then
			if [[ -e /usr/src/linux/.config  ]]; then mv --verbose --force /usr/src/linux/.config /usr/src/linux/.config--$(date +%Y-%m-%d--%H-%M-%S).bak; fi
			genkernel --color --menuconfig --save-config --bcache --no-hyperv --microcode=amd --no-virtio --mrproper --no-splash --no-ramdisk-modules --no-nice --no-mdadm --no-lvm --no-nfs --no-dmraid --e2fsprogs --no-xfsprogs --no-zfs --no-btrfs --disklabel --no-luks --busybox --no-unionfs --no-netboot --compress-initramfs --compress-initramfs-type=best --kernel-config=$(echo ${array[$select]} | cut -d ' ' -f9) all
		else
			echo -e "\nInvalid selection.\n"
		fi
	fi
else
	echo -e "\nBuild kernel using fresh archlinux config file or existing config file or select a config file from /etc/kernels.\n\n\tbuildkernel.sh <argument>\n\n\tfresh\t\tPull new archlinux config file and build kernel.\n\texisting\tUse existing config file (/usr/src/linux/.config) and build kernel.\n\tselect\t\tSelect from available kernel config files (/etc/kernels/) and build kernel.\n"
fi
