#!/bin/bash

# tar backup with gzip and checksum generation
tar --create --dereference --verbose --exclude='.keep*' --file - --files-from=/root/scripts/backup-files-list | pigz --best > /home/nvme0n1p4/backup-kvm.tar.gz
echo "$(sha512sum /home/nvme0n1p4/backup-kvm.tar.gz | cut -d ' ' -f1)  backup-kvm.tar.gz" > /home/nvme0n1p4/backup-kvm.tar.gz.sha512sum
